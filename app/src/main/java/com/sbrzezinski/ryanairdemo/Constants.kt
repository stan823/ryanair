package com.sbrzezinski.ryanairdemo

object Constants {
    const val BASE_URL = "https://www.ryanair.com/"
    const val STATIONS_URL = "https://tripstest.ryanair.com/"
}
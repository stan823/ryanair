package com.sbrzezinski.ryanairdemo

import android.app.Application
import com.sbrzezinski.ryanairdemo.data.di.dataModule
import com.sbrzezinski.ryanairdemo.domain.di.domainModule
import com.sbrzezinski.ryanairdemo.presentation.di.presentationModule
import com.sbrzezinski.ryanairdemo.system.di.systemModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class RyanAirDemoApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidContext(this@RyanAirDemoApplication)
            modules(
                systemModule,
                dataModule,
                domainModule,
                presentationModule
            )
        }
    }
}

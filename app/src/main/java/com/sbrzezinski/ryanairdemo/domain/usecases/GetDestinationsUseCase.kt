package com.sbrzezinski.ryanairdemo.domain.usecases

import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository
import com.sbrzezinski.ryanairdemo.domain.models.Stations
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class GetDestinationsUseCase(
    private val flightsRepository: FlightsRepository,
    private val originsSubject: BehaviorSubject<String>
) {

    operator fun invoke(): Observable<List<String>> {
        return Observable.combineLatest(
            listOf(
                flightsRepository.stations,
                originsSubject
            )
        ) {
            val stations = it[0] as Stations
            val selectedOrigin = it[1] as String
            if (selectedOrigin.isEmpty()) {
                stations.destinations.keys.sorted()
            } else {
                stations.origins[selectedOrigin].orEmpty().sorted()
            }
        }.distinct()
    }
}

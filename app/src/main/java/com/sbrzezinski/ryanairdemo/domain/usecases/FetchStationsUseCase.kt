package com.sbrzezinski.ryanairdemo.domain.usecases

import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository

class FetchStationsUseCase(
    private val flightsRepository: FlightsRepository
) {
    operator fun invoke() = flightsRepository.fetchStations()
}
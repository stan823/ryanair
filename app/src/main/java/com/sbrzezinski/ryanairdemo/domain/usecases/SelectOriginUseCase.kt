package com.sbrzezinski.ryanairdemo.domain.usecases

import io.reactivex.rxjava3.subjects.BehaviorSubject

class SelectOriginUseCase(
    private val originsSubject: BehaviorSubject<String>
) {
    operator fun invoke(origin: String) = originsSubject.onNext(origin)

}
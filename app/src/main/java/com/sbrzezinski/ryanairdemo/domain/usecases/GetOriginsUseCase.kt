package com.sbrzezinski.ryanairdemo.domain.usecases

import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository
import com.sbrzezinski.ryanairdemo.domain.models.Stations
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class GetOriginsUseCase(
    private val flightsRepository: FlightsRepository,
    private val destinationsSubject: BehaviorSubject<String>
) {

    operator fun invoke(): Observable<List<String>> {
        return Observable.combineLatest(
            listOf(
                flightsRepository.stations,
                destinationsSubject
            )
        ) {
            val stations = it[0] as Stations
            val selectedDestination = it[1] as String
            if (selectedDestination.isEmpty()) {
                stations.origins.keys.sorted()
            } else {
                stations.destinations[selectedDestination].orEmpty().sorted()
            }
        }.distinctUntilChanged()
    }


}
package com.sbrzezinski.ryanairdemo.domain.usecases

import android.util.Log
import io.reactivex.rxjava3.subjects.BehaviorSubject

class SelectDestinationUseCase(
    private val destinationsSubject: BehaviorSubject<String>
) {
    operator fun invoke(destination: String) = destinationsSubject.onNext(destination).also {
        Log.d("abc2","destination selected $destination")
    }

}
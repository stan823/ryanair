package com.sbrzezinski.ryanairdemo.domain.models

typealias StationsMap = Map<String, List<String>>

data class Stations(
    val origins: StationsMap,
    val destinations: StationsMap
)
package com.sbrzezinski.ryanairdemo.domain.usecases

import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository
import java.text.SimpleDateFormat
import java.util.*

class FetchFlightsUseCase(
    private val flightsRepository: FlightsRepository
) {
    operator fun invoke(
        date: Date,
        destination: String,
        origin: String,
        numberOfTeens: Int,
        numberOfAdults: Int,
        numberOfChildren: Int
    ) = flightsRepository.checkAvailability(
        dateOut = format.format(date),
        destination = destination,
        origin = origin,
        numberOfAdults = numberOfAdults,
        numberOfChildren = numberOfChildren,
        numberOfTeens = numberOfTeens
    )

    private companion object {
        val format = SimpleDateFormat("yyyy-MM-dd")
    }
}
package com.sbrzezinski.ryanairdemo.domain.usecases

import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class GetLoadingStateUseCase(
    private val loadingSubject: BehaviorSubject<Boolean>
) {
    operator fun invoke(): Observable<Boolean> = loadingSubject
}
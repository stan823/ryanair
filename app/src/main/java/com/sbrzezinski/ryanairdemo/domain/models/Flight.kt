package com.sbrzezinski.ryanairdemo.domain.models

import android.os.Parcelable
import androidx.recyclerview.widget.DiffUtil
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Flight(
    val date: String?,
    val duration: String?,
    val regularFare: Float?,
    val currency: String?,
    val fareClass: String?,
    val infantsLeft: Int?,
    val discountInPercentage: Float?,
    val flightNumber: String?,
    val origin: String?,
    val destination: String?
): Parcelable {

    companion object{
        val diffUtil = object: DiffUtil.ItemCallback<Flight>(){
            override fun areItemsTheSame(oldItem: Flight, newItem: Flight) = oldItem === newItem

            override fun areContentsTheSame(oldItem: Flight, newItem: Flight) = oldItem == newItem
        }
    }
}
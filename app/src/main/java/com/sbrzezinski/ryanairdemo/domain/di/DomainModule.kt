package com.sbrzezinski.ryanairdemo.domain.di

import com.sbrzezinski.ryanairdemo.domain.usecases.*
import io.reactivex.rxjava3.subjects.BehaviorSubject
import io.reactivex.rxjava3.subjects.PublishSubject
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module

val selectedOriginQualifier = StringQualifier("selectedOrigin")
val selectedDestinationQualifier = StringQualifier("selectedDestination")
val loadingQualifier = StringQualifier("loading")
val domainModule = module {

    single(loadingQualifier) { BehaviorSubject.createDefault(true) }
    single(selectedOriginQualifier) { BehaviorSubject.createDefault("") }
    single(selectedDestinationQualifier) { BehaviorSubject.createDefault("") }
    single {
        GetOriginsUseCase(
            flightsRepository = get(),
            destinationsSubject = get(selectedDestinationQualifier)
        )
    }
    single {
        GetDestinationsUseCase(
            flightsRepository = get(),
            originsSubject = get(selectedOriginQualifier)
        )
    }
    single { SelectDestinationUseCase(destinationsSubject = get(selectedDestinationQualifier)) }
    single { SelectOriginUseCase(originsSubject = get(selectedOriginQualifier)) }
    single { FetchFlightsUseCase(flightsRepository = get()) }
    single { FetchStationsUseCase(flightsRepository = get()) }
    single { GetFlightsUseCase(flightsRepository = get()) }
    single { GetLoadingStateUseCase(loadingSubject = get(loadingQualifier)) }
    single { ChangeLoadingStateUseCase(loadingSubject = get(loadingQualifier)) }
}

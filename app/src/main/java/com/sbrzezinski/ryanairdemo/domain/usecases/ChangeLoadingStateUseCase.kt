package com.sbrzezinski.ryanairdemo.domain.usecases

import io.reactivex.rxjava3.subjects.BehaviorSubject

class ChangeLoadingStateUseCase(
    private val loadingSubject: BehaviorSubject<Boolean>
) {
    operator fun invoke(isLoading: Boolean){
        loadingSubject.onNext(isLoading)
    }
}
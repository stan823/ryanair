package com.sbrzezinski.ryanairdemo.data.network

import com.sbrzezinski.ryanairdemo.data.models.StationsResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET

interface StationsService {

    @GET("static/stations.json")
    fun getStations(): Single<StationsResponse>
}
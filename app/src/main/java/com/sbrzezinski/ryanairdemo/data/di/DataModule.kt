package com.sbrzezinski.ryanairdemo.data.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.sbrzezinski.ryanairdemo.Constants
import com.sbrzezinski.ryanairdemo.data.mappers.AvailabilityResponseToFlightsMapper
import com.sbrzezinski.ryanairdemo.data.mappers.StationsResponseToStationsMapper
import com.sbrzezinski.ryanairdemo.data.network.FlightsService
import com.sbrzezinski.ryanairdemo.data.network.StationsService
import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository
import hu.akarnokd.rxjava3.retrofit.RxJava3CallAdapterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import org.koin.dsl.module
import retrofit2.Retrofit

private val retrofit = Retrofit
    .Builder()
    .baseUrl(Constants.BASE_URL)
    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
    .addConverterFactory(
        Json { ignoreUnknownKeys = true }
            .asConverterFactory(MediaType.get("application/json")))
    .build()

private val stationsRetrofit = Retrofit
    .Builder()
    .baseUrl(Constants.STATIONS_URL)
    .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
    .addConverterFactory(
        Json { ignoreUnknownKeys = true }
    .asConverterFactory(MediaType.get("application/json")))
    .build()



val dataModule = module {

    single { retrofit.create(FlightsService::class.java) }
    single { stationsRetrofit.create(StationsService::class.java) }
    single { AvailabilityResponseToFlightsMapper() }
    single { StationsResponseToStationsMapper() }
    single {
        FlightsRepository(
            flightsService = get(),
            stationsService = get(),
            availabilityToFlightsMapper = get(),
            stationsResponseToStationsMapper = get()
        )
    }
}

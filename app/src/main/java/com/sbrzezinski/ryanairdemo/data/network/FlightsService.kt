package com.sbrzezinski.ryanairdemo.data.network

import com.sbrzezinski.ryanairdemo.data.models.AvailabilityResponse
import com.sbrzezinski.ryanairdemo.data.models.StationsResponse
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface FlightsService {

    @GET("api/booking/v4/en-gb/Availability")
    fun checkAvailability(
        @Query("dateout") dateOut: String,
        @Query("origin") origin: String,
        @Query("destination") destination: String,
        @Query("adt") numberOfAdults: Int,
        @Query("chd") numberOfChildren: Int,
        @Query("teen") numberOfTeens: Int,

        @Query("inf") inf: Int = 0,
        @Query("Disc") Disc: Int = 0,
        @Query("flexdaysout") flexdaysout: Int = 3,
        @Query("roundtrip") roundtrip: Boolean = false,
        @Query("flexdaysbeforeout") flexdaysbeforeout: Int = 3,
        @Query("ToUs") ToUs: String = "AGREED",
    ): Single<AvailabilityResponse>

}
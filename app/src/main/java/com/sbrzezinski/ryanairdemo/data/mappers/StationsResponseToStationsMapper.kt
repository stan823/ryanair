package com.sbrzezinski.ryanairdemo.data.mappers

import com.sbrzezinski.ryanairdemo.data.models.StationsResponse
import com.sbrzezinski.ryanairdemo.domain.models.Stations

class StationsResponseToStationsMapper {


    /*
        As far as I understood, station->market is a route origin->destinations
     */
    fun map(stationsResponse: StationsResponse): Stations {

        val destinations = mutableMapOf<String, List<String>>()

        val origins = stationsResponse.stations.orEmpty().associate { station ->
            station.code.orEmpty() to station.markets?.mapNotNull {
                it?.code?.also { code ->
                    destinations[code] = (destinations[code] ?: listOf()).toMutableList()
                        .also { it.add(station.code.orEmpty()) }
                }
            }.orEmpty()
        }

        return Stations(origins, destinations)
    }
}
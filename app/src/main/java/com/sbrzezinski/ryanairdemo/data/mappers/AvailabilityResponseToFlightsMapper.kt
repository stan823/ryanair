package com.sbrzezinski.ryanairdemo.data.mappers

import com.sbrzezinski.ryanairdemo.data.models.AvailabilityResponse
import com.sbrzezinski.ryanairdemo.domain.models.Flight

class AvailabilityResponseToFlightsMapper {

    fun map(response: AvailabilityResponse): List<Flight> {
        return response.trips?.flatMap { trip ->
            trip?.dates.orEmpty().flatMap { date ->
                date?.flights.orEmpty().map { flight ->
                    val regularFare = flight?.regularFare


                    Flight(
                        // Here there were multiple dates in `timeUTC`, so I assumed that first one is the right one.
                        date = flight?.timeUTC?.first(),
                        duration = flight?.duration,

                        // I wasn't sure how to get this fare - I hope this is what it was about.
                        regularFare = regularFare?.fares?.firstOrNull()?.amount?.toFloat(),
                        currency = response.currency,
                        fareClass = regularFare?.fareClass,
                        infantsLeft = flight?.infantsLeft,
                        discountInPercentage = regularFare?.fares?.firstOrNull()?.discountInPercent?.toFloat(),
                        flightNumber = flight?.flightNumber,
                        origin = trip?.originName,
                        destination = trip?.destinationName
                    )
                }
            }
        }.orEmpty()
    }
}
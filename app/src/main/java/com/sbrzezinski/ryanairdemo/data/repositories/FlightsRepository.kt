package com.sbrzezinski.ryanairdemo.data.repositories

import com.sbrzezinski.ryanairdemo.data.mappers.AvailabilityResponseToFlightsMapper
import com.sbrzezinski.ryanairdemo.data.mappers.StationsResponseToStationsMapper
import com.sbrzezinski.ryanairdemo.data.network.FlightsService
import com.sbrzezinski.ryanairdemo.data.network.StationsService
import com.sbrzezinski.ryanairdemo.domain.models.Stations
import com.sbrzezinski.ryanairdemo.domain.models.StationsMap
import com.sbrzezinski.ryanairdemo.domain.models.Flight
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.subjects.BehaviorSubject

class FlightsRepository(
    private val flightsService: FlightsService,
    private val stationsService: StationsService,
    private val availabilityToFlightsMapper: AvailabilityResponseToFlightsMapper,
    private val stationsResponseToStationsMapper: StationsResponseToStationsMapper
) {

    private var _stations = BehaviorSubject.create<Stations>()
    val stations: Observable<Stations> get() = _stations

    private var _flightsFound = BehaviorSubject.create<List<Flight>>()
    val flightsFound: Observable<List<Flight>> get() = _flightsFound

    /**
     * We retrieve stations from endpoint and we map them to [StationsMap].
     * Result is stored in [storedStations], so that we don't have to request them over the network
     * each time
     */
    fun fetchStations(): Completable = stationsService.getStations().doOnSuccess { stations ->
        _stations.onNext(stationsResponseToStationsMapper.map(stations))
    }.ignoreElement()


    fun checkAvailability(
        dateOut: String,
        numberOfAdults: Int,
        numberOfChildren: Int,
        numberOfTeens: Int,
        destination: String,
        origin: String
    ): Completable = flightsService.checkAvailability(
        dateOut, origin, destination, numberOfAdults, numberOfChildren, numberOfTeens
    ).doOnSuccess { availabilityResponse ->
        _flightsFound.onNext(availabilityToFlightsMapper.map(availabilityResponse))
    }.ignoreElement()

}
package com.sbrzezinski.ryanairdemo.data.models


import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class StationsResponse(
    @SerialName("stations")
    val stations: List<StationResponse>? = null
)

@Serializable
data class StationResponse(
    @SerialName("alias")
    val alias: List<String?>? = null,
    @SerialName("alternateName")
    val alternateName: String? = null,
    @SerialName("code")
    val code: String? = null,
    @SerialName("countryAlias")
    val countryAlias: String? = null,
    @SerialName("countryCode")
    val countryCode: String? = null,
    @SerialName("countryGroupCode")
    val countryGroupCode: String? = null,
    @SerialName("countryGroupName")
    val countryGroupName: String? = null,
    @SerialName("countryName")
    val countryName: String? = null,
    @SerialName("latitude")
    val latitude: String? = null,
    @SerialName("longitude")
    val longitude: String? = null,
    @SerialName("markets")
    val markets: List<MarketResponse?>? = null,
    @SerialName("mobileBoardingPass")
    val mobileBoardingPass: Boolean? = null,
    @SerialName("name")
    val name: String? = null,
    @SerialName("notices")
    val notices: String? = null,
    @SerialName("timeZoneCode")
    val timeZoneCode: String? = null
)

@Serializable
data class MarketResponse(
    @SerialName("code")
    val code: String? = null,
    @SerialName("group")
    val group: String? = null
)

package com.sbrzezinski.ryanairdemo.data.models

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class AvailabilityResponse(
    @SerialName("currPrecision")
    val currPrecision: Int? = null,
    @SerialName("currency")
    val currency: String? = null,
    @SerialName("serverTimeUTC")
    val serverTimeUTC: String? = null,
    @SerialName("termsOfUse")
    val termsOfUse: String? = null,
    @SerialName("trips")
    val trips: List<TripResponse?>? = null
)

@Serializable
data class TripResponse(
    @SerialName("dates")
    val dates: List<DateResponse?>? = null,
    @SerialName("destination")
    val destination: String? = null,
    @SerialName("destinationName")
    val destinationName: String? = null,
    @SerialName("origin")
    val origin: String? = null,
    @SerialName("originName")
    val originName: String? = null
)

@Serializable
data class DateResponse(
    @SerialName("dateOut")
    val dateOut: String? = null,
    @SerialName("flights")
    val flights: List<FlightResponse?>? = null
)

@Serializable
data class FlightResponse(
    @SerialName("duration")
    val duration: String? = null,
    @SerialName("faresLeft")
    val faresLeft: Int? = null,
    @SerialName("flightKey")
    val flightKey: String? = null,
    @SerialName("flightNumber")
    val flightNumber: String? = null,
    @SerialName("infantsLeft")
    val infantsLeft: Int? = null,
    @SerialName("regularFare")
    val regularFare: RegularFareResponse? = null,
    @SerialName("segments")
    val segments: List<SegmentResponse?>? = null,
    @SerialName("time")
    val time: List<String?>? = null,
    @SerialName("timeUTC")
    val timeUTC: List<String?>? = null
)

@Serializable
data class RegularFareResponse(
    @SerialName("fareClass")
    val fareClass: String? = null,
    @SerialName("fareKey")
    val fareKey: String? = null,
    @SerialName("fares")
    val fares: List<FareResponse?>? = null
)

@Serializable
data class FareResponse(
    @SerialName("amount")
    val amount: Double? = null,
    @SerialName("count")
    val count: Int? = null,
    @SerialName("discountInPercent")
    val discountInPercent: Int? = null,
    @SerialName("hasDiscount")
    val hasDiscount: Boolean? = null,
    @SerialName("hasPromoDiscount")
    val hasPromoDiscount: Boolean? = null,
    @SerialName("publishedFare")
    val publishedFare: Double? = null,
    @SerialName("type")
    val type: String? = null
)


@Serializable
data class SegmentResponse(
    @SerialName("destination")
    val destination: String? = null,
    @SerialName("duration")
    val duration: String? = null,
    @SerialName("flightNumber")
    val flightNumber: String? = null,
    @SerialName("origin")
    val origin: String? = null,
    @SerialName("segmentNr")
    val segmentNr: Int? = null,
    @SerialName("time")
    val time: List<String?>? = null,
    @SerialName("timeUTC")
    val timeUTC: List<String?>? = null
)
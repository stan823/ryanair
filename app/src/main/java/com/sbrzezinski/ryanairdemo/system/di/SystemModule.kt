package com.sbrzezinski.ryanairdemo.system.di

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.schedulers.Schedulers
import org.koin.core.qualifier.StringQualifier
import org.koin.dsl.module

val mainThreadSchedulerQualifier = StringQualifier("mainThreadScheduler")
val ioSchedulerQualifier = StringQualifier("ioScheduler")

val systemModule = module {
    single<Scheduler>(mainThreadSchedulerQualifier) { AndroidSchedulers.mainThread() }
    single<Scheduler>(ioSchedulerQualifier) { Schedulers.io() }
}
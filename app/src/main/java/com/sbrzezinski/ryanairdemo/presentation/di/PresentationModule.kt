package com.sbrzezinski.ryanairdemo.presentation.di

import com.sbrzezinski.ryanairdemo.presentation.viewmodels.FlightsListFragmentViewModel
import com.sbrzezinski.ryanairdemo.presentation.viewmodels.HomeFragmentViewModel
import com.sbrzezinski.ryanairdemo.presentation.viewmodels.MainActivityViewModel
import com.sbrzezinski.ryanairdemo.system.di.ioSchedulerQualifier
import com.sbrzezinski.ryanairdemo.system.di.mainThreadSchedulerQualifier
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val presentationModule = module {
    viewModel {
        HomeFragmentViewModel(
            getAllDestinationsUseCase = get(),
            getAllOriginsUseCase = get(),
            fetchStationsUseCase = get(),
            fetchFlightsUseCase = get(),
            selectDestinationUseCase = get(),
            selectOriginUseCase = get(),
            mainScheduler = get(mainThreadSchedulerQualifier),
            ioScheduler = get(ioSchedulerQualifier),
            changeLoadingStateUseCase = get()
        )
    }

    viewModel {
        FlightsListFragmentViewModel(
            getFlightsUseCase = get()
        )
    }

    viewModel {
        MainActivityViewModel(
            getLoadingStateUseCase = get()
        )
    }
}
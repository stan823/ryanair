package com.sbrzezinski.ryanairdemo.presentation.ui.flightsadapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.sbrzezinski.ryanairdemo.R
import com.sbrzezinski.ryanairdemo.databinding.CellFlightBinding
import com.sbrzezinski.ryanairdemo.domain.models.Flight
import java.lang.Exception
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*


class FlightsAdapter(
    private val itemClickCallback: (Flight) -> Unit
) : ListAdapter<Flight, FlightViewHolder>(Flight.diffUtil) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FlightViewHolder {
        return FlightViewHolder(
            CellFlightBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: FlightViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.bind(item) { itemClickCallback(item) }
        }
    }
}

class FlightViewHolder(
    private val binding: CellFlightBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(flight: Flight, itemClickCallback: () -> Unit) {
        with(binding) {

            binding.container.setOnClickListener {
                itemClickCallback()
            }

            flightNumber.text = flight.flightNumber
            flightNumber.isVisible = flight.flightNumber != null

            flightDate.text = try {
                inputDateFormatter.parse(flight.date.orEmpty())?.let { date ->
                    flightDate.isVisible = true
                    outputDateFormatter.format(date)
                }
            } catch (e: Exception) {
                flightDate.isVisible = false
                null
            }
            inputDateFormatter
            flightDuration.text = flight.duration
            flightDuration.isVisible = flight.duration != null

            flightPrice.text = itemView.context.getString(
                R.string.price_label,
                flight.regularFare ?: 0.0f,
                flight.currency.orEmpty()
            )

        }
    }

    private companion object {
        val inputDateFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
            .also { it.timeZone = TimeZone.getTimeZone("UTC") }
        val outputDateFormatter =
            SimpleDateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT)
                .also { it.timeZone = TimeZone.getDefault() }
    }
}
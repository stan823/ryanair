package com.sbrzezinski.ryanairdemo.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sbrzezinski.ryanairdemo.domain.usecases.GetLoadingStateUseCase

class MainActivityViewModel(
    getLoadingStateUseCase: GetLoadingStateUseCase
) : ViewModel(){

    private val _isLoading = MutableLiveData(true)
    val isLoading: LiveData<Boolean> get() = _isLoading

    private val disposable = getLoadingStateUseCase()
        .subscribe(_isLoading::postValue)

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}
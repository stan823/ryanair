package com.sbrzezinski.ryanairdemo.presentation.ui

import android.app.Activity
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.sbrzezinski.ryanairdemo.R
import com.sbrzezinski.ryanairdemo.databinding.FragmentHomeBinding
import com.sbrzezinski.ryanairdemo.presentation.models.ToolbarData
import com.sbrzezinski.ryanairdemo.presentation.viewmodels.HomeFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*


class HomeFragment : Fragment() {

    private val viewModel by viewModel<HomeFragmentViewModel>()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentHomeBinding.inflate(layoutInflater)


        binding.origin.setAdapter(createAdapter(listOf("-")))
        binding.destination.setAdapter(createAdapter(listOf("-")))

        binding.origin.doOnTextChanged { text, start, before, count ->
            if (count == 3) {
                viewModel.onOriginChanged(text.toString())
            } else {
                viewModel.onOriginChanged("")
            }
        }
        binding.destination.doOnTextChanged { text, start, before, count ->
            if (count == 3) {
                viewModel.onDestinationChanged(text.toString())
            } else {
                viewModel.onDestinationChanged("")
            }
        }
        binding.selectDateButton.setOnClickListener {
            val calendar = Calendar.getInstance()
            DatePickerDialog(
                requireContext(),
                { _, year, month, day -> viewModel.onDateChanged(year, month, day) },
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH),
            ).show()
        }
        binding.teens.doOnTextChanged { text, _, _, _ ->
            viewModel.teens = text.toString().toIntOrNull() ?: 0
        }

        binding.adults.doOnTextChanged { text, _, _, _ ->
            viewModel.adults = text.toString().toIntOrNull() ?: 0
        }

        binding.children.doOnTextChanged { text, _, _, _ ->
            viewModel.children = text.toString().toIntOrNull() ?: 0
        }

        binding.submit.setOnClickListener { viewModel.submit() }

        viewModel.destinations.observe(viewLifecycleOwner) { destinations ->
            binding.destination.setAdapter(createAdapter(destinations))
        }

        viewModel.origins.observe(viewLifecycleOwner) { origins ->
            binding.origin.setAdapter(createAdapter(origins))
        }

        viewModel.showError.observe(viewLifecycleOwner) {
            Toast.makeText(context, R.string.something_went_wrong, Toast.LENGTH_LONG).show()
        }

        viewModel.dateTooEarlyError.observe(viewLifecycleOwner) {
            Toast.makeText(context, R.string.flight_date_cannot_be_earlier, Toast.LENGTH_LONG)
                .show()
        }

        viewModel.submitEnabled.observe(viewLifecycleOwner) { isEnabled ->
            binding.submit.isEnabled = isEnabled
        }
        viewModel.dateLabel.observe(viewLifecycleOwner) {
            if (it != null) binding.selectDateButton.text = it
        }

        viewModel.navigateToDetails.observe(viewLifecycleOwner) {
            (activity as? AppCompatActivity)?.supportActionBar?.title =
                "${binding.origin.text} -> ${binding.destination.text}"
            findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToFlightsListFragment(
                    ToolbarData(
                        origin = binding.origin.text.toString(),
                        destinations = binding.destination.text.toString()
                    )
                )
            )
        }


        return binding.root
    }

    private fun createAdapter(destinations: List<String>): ArrayAdapter<String> {
        return ArrayAdapter(
            requireContext(),
            android.R.layout.simple_spinner_dropdown_item,
            destinations
        )
    }

    override fun onResume() {
        super.onResume()
        (activity as? AppCompatActivity)?.supportActionBar?.setTitle(R.string.app_name)
    }

    override fun onPause() {
        super.onPause()
        (context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow(view?.windowToken, 0)

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
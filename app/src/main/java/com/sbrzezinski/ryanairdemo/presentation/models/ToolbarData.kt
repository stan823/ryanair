package com.sbrzezinski.ryanairdemo.presentation.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ToolbarData(
    val origin: String,
    val destinations: String
): Parcelable
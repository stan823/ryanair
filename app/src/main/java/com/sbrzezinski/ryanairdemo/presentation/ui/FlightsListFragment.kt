package com.sbrzezinski.ryanairdemo.presentation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.sbrzezinski.ryanairdemo.R
import com.sbrzezinski.ryanairdemo.databinding.FragmentFlightsListBinding
import com.sbrzezinski.ryanairdemo.domain.models.Flight
import com.sbrzezinski.ryanairdemo.presentation.ui.flightsadapter.FlightsAdapter
import com.sbrzezinski.ryanairdemo.presentation.viewmodels.FlightsListFragmentViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class FlightsListFragment : Fragment() {

    private val navArgs: FlightsListFragmentArgs by navArgs()
    private var _binding: FragmentFlightsListBinding? = null
    private val binding: FragmentFlightsListBinding
        get() = _binding!!

    private val adapter by lazy { FlightsAdapter(::showDetails) }
    private val viewModel by viewModel<FlightsListFragmentViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentFlightsListBinding.inflate(layoutInflater)

        binding.flightsRecycler.layoutManager = LinearLayoutManager(requireContext())
        binding.flightsRecycler.adapter = adapter
        binding.priceSlider.apply {
            addOnChangeListener { _, value, _ ->
                binding.sliderLabel.text = getString(R.string.please_select_minimum_price, value)
                viewModel.onMaxPriceChanged(value)
            }
            valueFrom = 0f
            valueTo = 1000f
            value = 150f
        }

        viewModel.flights.observe(viewLifecycleOwner) { flights ->
            adapter.submitList(flights)
        }

        (activity as? AppCompatActivity)?.supportActionBar?.title =
            "${navArgs.toolbarData.origin} -> ${navArgs.toolbarData.destinations}"
        return binding.root
    }

    private fun showDetails(flight: Flight) {
        findNavController()
            .navigate(
                FlightsListFragmentDirections.actionFlightsListFragmentToDetailsFragment(flight, navArgs.toolbarData)
            )
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
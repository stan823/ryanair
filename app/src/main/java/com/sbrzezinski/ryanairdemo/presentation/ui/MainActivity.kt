package com.sbrzezinski.ryanairdemo.presentation.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import androidx.core.view.isVisible
import com.sbrzezinski.ryanairdemo.R
import com.sbrzezinski.ryanairdemo.databinding.ActivityMainBinding
import com.sbrzezinski.ryanairdemo.presentation.viewmodels.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        viewModel.isLoading.observe(this) { isLoading ->
            binding.loadingBar.isVisible = isLoading
        }
        setContentView(binding.root)


    }
}
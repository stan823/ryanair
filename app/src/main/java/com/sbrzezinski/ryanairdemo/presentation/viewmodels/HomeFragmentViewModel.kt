package com.sbrzezinski.ryanairdemo.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.sbrzezinski.ryanairdemo.domain.usecases.*
import com.sbrzezinski.ryanairdemo.utils.SingleLiveEvent
import io.reactivex.rxjava3.core.ObservableTransformer
import io.reactivex.rxjava3.core.Scheduler
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.kotlin.plusAssign
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class HomeFragmentViewModel(
    getAllOriginsUseCase: GetOriginsUseCase,
    getAllDestinationsUseCase: GetDestinationsUseCase,
    fetchStationsUseCase: FetchStationsUseCase,
    private val fetchFlightsUseCase: FetchFlightsUseCase,
    private val selectDestinationUseCase: SelectDestinationUseCase,
    private val selectOriginUseCase: SelectOriginUseCase,
    private val mainScheduler: Scheduler,
    private val ioScheduler: Scheduler,
    private val changeLoadingStateUseCase: ChangeLoadingStateUseCase
) : ViewModel() {

    var adults: Int = 0
        set(value) {
            field = value
            validateFields()
        }

    var teens: Int = 0
        set(value) {
            field = value
            validateFields()
        }

    var children: Int = 0
        set(value) {
            field = value
            validateFields()
        }

    private var selectedDestination = ""
    private var selectedOrigin = ""

    private val _navigateToDetails = SingleLiveEvent<Unit>()
    val navigateToDetails: LiveData<Unit> = _navigateToDetails

    private val _submitEnabled = MutableLiveData<Boolean>(false)
    val submitEnabled: LiveData<Boolean> get() = _submitEnabled

    private val _destinations = MutableLiveData(emptyList<String>())
    val destinations: LiveData<List<String>> get() = _destinations

    private val _origins = MutableLiveData(emptyList<String>())
    val origins: LiveData<List<String>> get() = _origins

    private val _showError = SingleLiveEvent<Unit>()
    val showError: LiveData<Unit> get() = _showError

    private val _date = MutableLiveData<Date>()
    val dateLabel: LiveData<String> = Transformations.map(_date) { date ->
        if (date != null) {
            SimpleDateFormat.getDateInstance(DateFormat.MEDIUM).format(date)
        } else null
    }

    private val _dateTooEarlyError = SingleLiveEvent<Unit>()
    val dateTooEarlyError: LiveData<Unit> get() = _dateTooEarlyError

    private val disposable = CompositeDisposable()

    private val stationsTransformer
        get() = ObservableTransformer<List<String>, List<String>> { upstream ->
            upstream.observeOn(mainScheduler)
                .subscribeOn(ioScheduler)
                .map {
                    it.toMutableList().also { it.add(0, "---") }
                }
        }


    init {
        disposable += getAllOriginsUseCase()
            .compose(stationsTransformer)
            .subscribe(_origins::postValue) {
                _showError.postValue(Unit)
            }

        disposable += getAllDestinationsUseCase()
            .compose(stationsTransformer)
            .subscribe(_destinations::postValue) {
                _showError.postValue(Unit)
            }

        disposable += fetchStationsUseCase()
            .subscribeOn(ioScheduler)
            .observeOn(mainScheduler)
            .doOnSubscribe { changeLoadingStateUseCase(isLoading = true) }
            .doOnTerminate { changeLoadingStateUseCase(isLoading = false) }
            .subscribe({}, {
                _showError.call()
            })
    }


    fun onDestinationChanged(destination: String) {
        selectDestinationUseCase(destination)
        selectedDestination = destination
        validateFields()
    }

    fun onOriginChanged(origin: String) {
        selectOriginUseCase(origin)
        selectedOrigin = origin
        validateFields()
    }

    fun onDateChanged(year: Int, month: Int, day: Int) {
        val selectedDateCalendar = Calendar.getInstance()
        selectedDateCalendar.set(year, month, day)

        if (!selectedDateCalendar.before(Calendar.getInstance())) {
            _date.value = selectedDateCalendar.time
        } else _dateTooEarlyError.call()
        validateFields()
    }

    private fun validateFields() {
        _submitEnabled.postValue(
            _date.value != null &&
                    (adults + teens + children) > 0 &&
                    selectedDestination.length == 3 &&
                    selectedOrigin.length == 3
        )
    }

    fun submit() {
        _date.value?.let { date ->
            disposable += fetchFlightsUseCase(
                date = date,
                destination = selectedDestination,
                origin = selectedOrigin,
                numberOfTeens = teens,
                numberOfAdults = adults,
                numberOfChildren = children
            ).subscribeOn(ioScheduler)
                .observeOn(mainScheduler)
                .doOnSubscribe { changeLoadingStateUseCase(isLoading = true) }
                .doOnTerminate { changeLoadingStateUseCase(isLoading = false) }
                .subscribe({
                    _navigateToDetails.call()
                }, {
                    _showError.call()
                })
        }

    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}
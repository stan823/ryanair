package com.sbrzezinski.ryanairdemo.presentation.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.fragment.navArgs
import com.sbrzezinski.ryanairdemo.R
import com.sbrzezinski.ryanairdemo.databinding.FragmentDetailsBinding
import com.sbrzezinski.ryanairdemo.databinding.FragmentFlightsListBinding


class DetailsFragment : Fragment() {

    private val navArgs: DetailsFragmentArgs by navArgs()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentDetailsBinding.inflate(layoutInflater)

        with(binding) {
            origin.text = getString(R.string.from, navArgs.flight.origin.orEmpty())
            destination.text = getString(R.string.to, navArgs.flight.destination.orEmpty())
            infants.text = getString(R.string.infants_left, navArgs.flight.infantsLeft ?: 0)
            discount.text = getString(R.string.discount_percent, navArgs.flight.discountInPercentage ?: 0f)
            fareClass.text = getString(R.string.fare_class, navArgs.flight.fareClass.orEmpty())
        }

        (activity as? AppCompatActivity)?.supportActionBar?.title =
            "${navArgs.toolbarData.origin} -> ${navArgs.toolbarData.destinations}"

        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
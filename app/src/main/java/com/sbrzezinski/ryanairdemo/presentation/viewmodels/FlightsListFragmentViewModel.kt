package com.sbrzezinski.ryanairdemo.presentation.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.sbrzezinski.ryanairdemo.domain.models.Flight
import com.sbrzezinski.ryanairdemo.domain.usecases.GetFlightsUseCase


class FlightsListFragmentViewModel(
    private val getFlightsUseCase: GetFlightsUseCase
) : ViewModel() {


    private val maxPrice = MutableLiveData<Float>(0f)
    private val flightsList = MutableLiveData<List<Flight>>()
    private val _flights = MediatorLiveData<List<Flight>>()
        .apply {
            addSource(maxPrice) { value = filterFlights() }
            addSource(flightsList) { value = filterFlights() }
        }


    val flights: LiveData<List<Flight>> get() = _flights

    private fun filterFlights(): List<Flight> {
        return flightsList.value?.filter {
            (it.regularFare ?: 0f) <= (maxPrice.value ?: 0f)
        }.orEmpty()
    }

    private var disposable = getFlightsUseCase()
        .subscribe(flightsList::postValue)


    fun onMaxPriceChanged(price: Float) {
        maxPrice.value = price
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

}
package com.sbrzezinski.ryanairdemo.data.mappers

import com.sbrzezinski.ryanairdemo.data.models.*
import junit.framework.Assert.assertEquals
import junit.framework.TestCase
import org.junit.Test

class AvailabilityResponseToFlightsMapperTest {

    private val mapper = AvailabilityResponseToFlightsMapper()
    @Test
    fun `Given two Trips with two dates with two flights each are provided Then it should map to eight Flights`() {

        // Given
        val response = AvailabilityResponse(
            currency = "EUR",
            trips = listOf(
                TripResponse(
                    originName = "Wroclaw",
                    destinationName = "Dublin",
                    dates = listOf(
                        DateResponse(
                            flights = listOf(
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 3,
                                    duration = "1:20",
                                    regularFare = RegularFareResponse(
                                         fareClass = "fareClass1",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 10.0,
                                                discountInPercent = 15
                                            )
                                        )
                                    )
                                ),
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 3,
                                    duration = "1:50",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass2",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 20.0,
                                                discountInPercent = 16
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        DateResponse(
                            flights = listOf(
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 6,
                                    duration = "1:40",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass3",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 11.0,
                                                discountInPercent = 18
                                            )
                                        )
                                    )
                                ),
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 31,
                                    duration = "2:20",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass4",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 1.0,
                                                discountInPercent = 5
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                ),
                TripResponse(
                    originName = "Berlin",
                    destinationName = "London",
                    dates = listOf(
                        DateResponse(
                            flights = listOf(
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 34,
                                    duration = "1:25",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass5",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 9.0,
                                                discountInPercent = 1
                                            )
                                        )
                                    )
                                ),
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 6,
                                    duration = "1:20",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass6",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 107.0,
                                                discountInPercent = 12
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        DateResponse(
                            flights = listOf(
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 1,
                                    duration = "1:21",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass7",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 1.0,
                                                discountInPercent = 1
                                            )
                                        )
                                    )
                                ),
                                FlightResponse(
                                    timeUTC = listOf("2021-03-20T10:10:00.000Z"),
                                    infantsLeft = 30,
                                    duration = "1:23",
                                    regularFare = RegularFareResponse(
                                        fareClass = "fareClass8",
                                        fares = listOf(
                                            FareResponse(
                                                amount = 40.0,
                                                discountInPercent = 45
                                            )
                                        )
                                    )
                                )
                            )
                        )
                    )
                )
            )
        )


        // When
        val result = mapper.map(response)

        // Then
        assertEquals(8, result.size)

        result[0].let { flight ->
            assertEquals("Wroclaw", flight.origin)
            assertEquals("Dublin", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(3, flight.infantsLeft)
            assertEquals("fareClass1", flight.fareClass)
            assertEquals(10.0f, flight.regularFare)
            assertEquals(15.0f, flight.discountInPercentage)
            assertEquals("1:20", flight.duration)
        }

        result[1].let { flight ->
            assertEquals("Wroclaw", flight.origin)
            assertEquals("Dublin", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(3, flight.infantsLeft)
            assertEquals("fareClass2", flight.fareClass)
            assertEquals(20.0f, flight.regularFare)
            assertEquals(16.0f, flight.discountInPercentage)
            assertEquals("1:50", flight.duration)
        }

        result[2].let { flight ->
            assertEquals("Wroclaw", flight.origin)
            assertEquals("Dublin", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(6, flight.infantsLeft)
            assertEquals("fareClass3", flight.fareClass)
            assertEquals(11.0f, flight.regularFare)
            assertEquals(18.0f, flight.discountInPercentage)
            assertEquals("1:40", flight.duration)
        }

        result[3].let { flight ->
            assertEquals("Wroclaw", flight.origin)
            assertEquals("Dublin", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(31, flight.infantsLeft)
            assertEquals("fareClass4", flight.fareClass)
            assertEquals(1.0f, flight.regularFare)
            assertEquals(5.0f, flight.discountInPercentage)
            assertEquals("2:20", flight.duration)
        }

        result[4].let { flight ->
            assertEquals("Berlin", flight.origin)
            assertEquals("London", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(34, flight.infantsLeft)
            assertEquals("fareClass5", flight.fareClass)
            assertEquals(9f, flight.regularFare)
            assertEquals(1f, flight.discountInPercentage)
            assertEquals("1:25", flight.duration)
        }

        result[5].let { flight ->
            assertEquals("Berlin", flight.origin)
            assertEquals("London", flight.destination)
            assertEquals("EUR", flight.currency)
            assertEquals(6, flight.infantsLeft)
            assertEquals("fareClass6", flight.fareClass)
            assertEquals(107.0f, flight.regularFare)
            assertEquals(12.0f, flight.discountInPercentage)
            assertEquals("1:20", flight.duration)
        }
    }
}
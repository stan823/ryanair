package com.sbrzezinski.ryanairdemo.data.mappers

import com.sbrzezinski.ryanairdemo.data.models.MarketResponse
import com.sbrzezinski.ryanairdemo.data.models.StationResponse
import com.sbrzezinski.ryanairdemo.data.models.StationsResponse
import junit.framework.Assert.assertEquals
import org.junit.Test

class StationsResponseToStationsMapperTest {

    private val mapper = StationsResponseToStationsMapper()

    @Test
    fun `Given response provided Then it should map to proper statinos`() {

        // Given
        val stationsResponse = StationsResponse(
            stations = listOf(
                StationResponse(
                    code = "WRO",
                    markets = listOf(
                        MarketResponse(code = "DUB"),
                        MarketResponse(code = "BER"),
                        MarketResponse(code = "GOR"),
                    )
                ),
                StationResponse(
                    code = "POZ",
                    markets = listOf(
                        MarketResponse(code = "DUB"),
                        MarketResponse(code = "BER"),
                        MarketResponse(code = "WRO"),
                    )
                )
            )
        )

        // When
        val (origins, destinations) = mapper.map(stationsResponse)

        // Then
        assertEquals(2, origins.size)
        assertEquals(origins["WRO"], listOf("DUB", "BER", "GOR"))
        assertEquals(origins["POZ"], listOf("DUB", "BER", "WRO"))

        assertEquals(destinations["WRO"], listOf("POZ"))
        assertEquals(destinations["DUB"], listOf("WRO","POZ"))

    }
}
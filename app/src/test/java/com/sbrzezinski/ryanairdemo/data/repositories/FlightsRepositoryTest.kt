package com.sbrzezinski.ryanairdemo.data.repositories

import com.sbrzezinski.ryanairdemo.data.mappers.AvailabilityResponseToFlightsMapper
import com.sbrzezinski.ryanairdemo.data.mappers.StationsResponseToStationsMapper
import com.sbrzezinski.ryanairdemo.data.models.AvailabilityResponse
import com.sbrzezinski.ryanairdemo.data.models.StationsResponse
import com.sbrzezinski.ryanairdemo.data.network.FlightsService
import com.sbrzezinski.ryanairdemo.data.network.StationsService
import com.sbrzezinski.ryanairdemo.domain.models.Flight
import com.sbrzezinski.ryanairdemo.domain.models.Stations
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.reactivex.rxjava3.core.Single
import org.junit.Before
import org.junit.Test
import java.lang.Exception

class FlightsRepositoryTest {

    @RelaxedMockK
    private lateinit var flightsService: FlightsService

    @RelaxedMockK
    private lateinit var stationsService: StationsService

    @RelaxedMockK
    private lateinit var availabilityToFlightsMapper: AvailabilityResponseToFlightsMapper

    @RelaxedMockK
    private lateinit var stationsResponseToStationsMapper: StationsResponseToStationsMapper

    private val sut by lazy {
        FlightsRepository(
            flightsService,
            stationsService,
            availabilityToFlightsMapper,
            stationsResponseToStationsMapper
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `Given flightService succeeds When availability is checked Then flightFound should be updated`() {

        // Given
        val availabilityResponse = mockk<AvailabilityResponse>()
        val flightsList = listOf<Flight>(
            mockk(),
            mockk()
        )

        every {
            flightsService.checkAvailability("date",
                "origin",
                "destination",
                1,
                2,
                3
            )
        } returns Single.just(availabilityResponse)

        every {
            availabilityToFlightsMapper.map(availabilityResponse)
        } returns flightsList

        // When
        val testObserver = sut.flightsFound.test()
        sut.checkAvailability(
            "date",
            1,
            2,
            3,
            "destination",
            "origin"
        ).subscribe()

        // Then
        testObserver.assertValue(flightsList)
    }

    @Test
    fun `Given flightService succeeds When availability is checked Then it should complete`() {

        // Given
        val availabilityResponse = mockk<AvailabilityResponse>()
        val flightsList = listOf<Flight>(
            mockk(),
            mockk()
        )

        every {
            flightsService.checkAvailability("date",
                "origin",
                "destination",
                1,
                2,
                3
            )
        } returns Single.just(availabilityResponse)

        every {
            availabilityToFlightsMapper.map(availabilityResponse)
        } returns flightsList

        // When
        val testObserver = sut.checkAvailability(
            "date",
            1,
            2,
            3,
            "destination",
            "origin"
        ).test()

        // Then
        testObserver.assertComplete()
    }


    @Test
    fun `Given flightService fails When availability is checked Then flightFound should not be updated`() {

        // Given
        val availabilityResponse = mockk<AvailabilityResponse>()
        val flightsList = listOf<Flight>(
            mockk(),
            mockk()
        )

        every {
            flightsService.checkAvailability("date",
                "origin",
                "destination",
                1,
                2,
                3
            )
        } returns Single.error(Exception())

        // When
        val testObserver = sut.flightsFound.test()
        sut.checkAvailability(
            "date",
            1,
            2,
            3,
            "destination",
            "origin"
        ).subscribe({},{})

        // Then
        testObserver.assertNoValues()
    }

    @Test
    fun `Given flightService fails When availability is checked Then error should be emitted`() {

        // Given
        val exception = Exception()

        every {
            flightsService.checkAvailability("date",
                "origin",
                "destination",
                1,
                2,
                3
            )
        } returns Single.error(exception)

        // When
        val testObserver =sut.checkAvailability(
            "date",
            1,
            2,
            3,
            "destination",
            "origin"
        ).test()

        // Then
        testObserver.assertError(exception)
    }

    @Test
    fun `Given stations fetch succeeds Then stations should be updated and fetch completed`() {

        // Given
        val stationsResponse = mockk<StationsResponse>()
        val stations = mockk<Stations>()
        every {
            stationsService.getStations()
        } returns Single.just(stationsResponse)

        every {
            stationsResponseToStationsMapper.map(stationsResponse)
        } returns stations

        // When
        val stationsObserver = sut.stations.test()
        val fetchObserver = sut.fetchStations().test()

        // Then
        stationsObserver.assertValue(stations)
        fetchObserver.assertComplete()
    }

    @Test
    fun `Given stations fetch fails Then stations should not be updated and fetch not completed`() {

        // Given
        val exception = Exception()
        every {
            stationsService.getStations()
        } returns Single.error(exception)

        // When
        val stationsObserver = sut.stations.test()
        val fetchObserver = sut.fetchStations().test()

        // Then
        stationsObserver.assertNoErrors()
        stationsObserver.assertNoValues()
        fetchObserver.assertError(exception)
    }
}
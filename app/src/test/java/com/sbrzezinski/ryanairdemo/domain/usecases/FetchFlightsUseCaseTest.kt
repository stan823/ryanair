package com.sbrzezinski.ryanairdemo.domain.usecases

import com.sbrzezinski.ryanairdemo.data.repositories.FlightsRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import io.reactivex.rxjava3.core.Completable
import org.junit.Before
import org.junit.Test
import java.lang.Exception
import java.util.*

class FetchFlightsUseCaseTest {

    @RelaxedMockK
    private lateinit var flightsRepository: FlightsRepository

    private val sut by lazy { FetchFlightsUseCase(flightsRepository) }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `Given checking availability succeeds Then invoking usecase succeeds as well`() {

        // Given
        val calendar = Calendar.getInstance()
        calendar.set(2012, 4,10)
        val date = calendar.time

        every {
            flightsRepository.checkAvailability(
                dateOut = any(),
                destination = any(),
                origin = any(),
                numberOfAdults = any(),
                numberOfTeens = any(),
                numberOfChildren = any()
            )
        } returns Completable.complete()

        // When
        val testObsever = sut(
            date = date,
            destination = "destination",
            origin = "origin",
            numberOfAdults = 1,
            numberOfTeens = 2,
            numberOfChildren = 3
        ).test()

        // Then
        verify {
            flightsRepository.checkAvailability(
                dateOut = "2012-05-10",
                destination = "destination",
                origin = "origin",
                numberOfAdults = 1,
                numberOfTeens = 2,
                numberOfChildren = 3
            )
        }
        testObsever.assertComplete()
    }


    @Test
    fun `Given checking availability fails Then invoking usecase fails as well`() {

        // Given
        val calendar = Calendar.getInstance()
        calendar.set(2012, 4,10)
        val date = calendar.time
        val exception = Exception()

        every {
            flightsRepository.checkAvailability(
                dateOut = any(),
                destination = any(),
                origin = any(),
                numberOfAdults = any(),
                numberOfTeens = any(),
                numberOfChildren = any()
            )
        } returns Completable.error(exception)

        // When
        val testObsever = sut(
            date = date,
            destination = "destination",
            origin = "origin",
            numberOfAdults = 1,
            numberOfTeens = 2,
            numberOfChildren = 3
        ).test()

        // Then
        verify {
            flightsRepository.checkAvailability(
                dateOut = "2012-05-10",
                destination = "destination",
                origin = "origin",
                numberOfAdults = 1,
                numberOfTeens = 2,
                numberOfChildren = 3
            )
        }
        testObsever.assertError(exception)
    }
}